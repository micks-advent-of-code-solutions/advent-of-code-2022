# Advent of Code Framework

## Usage

`npm start [dayNumber]` e.g. `npm start 1`

To start implementing that day. If the needed files do not exist, they will be created for you. nodemon is used to rerun code on changes.

## Structure

### `input / Day[x].txt`

Input used to run the solution with

### `solution / Day[x].js`

Implentation of the Solution

Just implement `solveSilver` and `solveGold` respectivly :)
