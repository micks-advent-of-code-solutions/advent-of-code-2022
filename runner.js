import { readFile, writeFile, stat } from "fs/promises";

async function replacePlaceholder(file, day) {
  const realFile = file.replace(/__DAY__/g, day);
  const fileExists = await stat(realFile)
    .then((x) => true)
    .catch((x) => false);
  if (!fileExists) {
    const originalContent = await readFile(file, "utf8");
    const realContent = originalContent.replace(/__DAY__/g, day);
    const realFile = file.replace(/__DAY__/g, day);
    await writeFile(realFile, realContent, "utf8");
  }
}

const solutionArg = process.argv[2];
let solution;
if (parseInt(solutionArg).toString() === solutionArg) {
  solution = "Day" + solutionArg;
}

if (!solution) {
  console.log("Please provide the solution you want to run as following: '1'");
  process.exit(0);
}

(async () => {
  try {
    const SolutionModule = (await import("./solution/" + solution + ".js"))
      .default;
    new SolutionModule();
  } catch (err) {
    console.log("Solution doesn't exist yet or has errors");
    await Promise.all([
      replacePlaceholder("solution/__DAY__.js", solution),
      replacePlaceholder("input/__DAY__.txt", solution),
    ]);
  }
})();
