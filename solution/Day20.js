import Solution from "./Solution.js";
import { mapInt } from "./utils.js";

export default class Day20 extends Solution {
  async prepareData(input, isSilver, isGold) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input.split("\n").map(mapInt);
  }
  async solveSilver(data) {
    return data;
  }

  async solveGold(data) {
    return "Not solved Yet!";
  }
}
