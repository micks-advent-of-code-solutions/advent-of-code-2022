import Solution from "./Solution.js";
import { toProduct } from "./utils.js";

function visibility(treeHeight, sightLine) {
  let visibility = sightLine.length;
  for (let i = 0; i < sightLine.length; i++) {
    if (sightLine[i] >= treeHeight) {
      visibility = i + 1;
      break;
    }
  }
  return visibility;
}

export default class Day8 extends Solution {
  async prepareData(input) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    const data = input
      .split("\n")
      .map((line) => line.split("").map((tree) => parseInt(tree)));

    const width = data[0].length;
    const height = data.length;

    const transposed = Array(width)
      .fill()
      .map(() => Array(height).fill());

    for (let x = 0; x < width; x++) {
      for (let y = 0; y < height; y++) {
        transposed[x][y] = data[y][x];
      }
    }

    return { data, transposed, width, height };
  }
  async solveSilver({ data, transposed, width, height }) {
    let visibleTrees = width * 2 + (height - 2) * 2;
    for (let y = 1; y < height - 1; y++) {
      for (let x = 1; x < width - 1; x++) {
        const treeToCheck = data[y][x];

        const sightLines = [
          data[y].slice(0, x).reverse(),
          data[y].slice(x + 1),
          transposed[x].slice(0, y).reverse(),
          transposed[x].slice(y + 1),
        ];

        if (sightLines.some((sl) => Math.max(...sl) < treeToCheck)) {
          visibleTrees++;
        }
      }
    }
    return visibleTrees;
  }

  async solveGold({ data, transposed, width, height }) {
    let bestView = 0;

    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        const treeToCheck = data[y][x];

        const sightLines = [
          data[y].slice(0, x).reverse(),
          data[y].slice(x + 1),
          transposed[x].slice(0, y).reverse(),
          transposed[x].slice(y + 1),
        ];

        const visibilityScore = sightLines
          .map((sl) => visibility(treeToCheck, sl))
          .reduce(toProduct);

        bestView = Math.max(visibilityScore, bestView);
      }
    }

    return bestView;
  }
}
