import Solution from "./Solution.js";
import { manhattanDistance, mapInt, rangeOf } from "./utils.js";

const renderLine = (signalSensorPairs, y, xFrom, xTo) =>
  rangeOf(xTo + 1, xFrom)
    .map((x) => {
      for (let pair of signalSensorPairs) {
        if (pair.sensor[0] === x && pair.sensor[1] === y) {
          return "S";
        }
        if (pair.beacon[0] === x && pair.beacon[1] === y) {
          return "B";
        }
        if (pair.distance >= manhattanDistance(pair.sensor, [x, y])) {
          return "#";
        }
      }
      return ".";
    })
    .join("");

export default class Day15 extends Solution {
  async prepareData(input, isSilver, isGold) {
    return input
      .split("\n")
      .map((line) =>
        line
          .match(/x=(-?\d+), y=(-?\d+).*?x=(-?\d+), y=(-?\d+)/)
          .slice(1, 5)
          .map(mapInt)
      )
      .map(([sx, sy, bx, by]) => ({ sensor: [sx, sy], beacon: [bx, by] }))
      .map((pair) => ({
        ...pair,
        distance: manhattanDistance(pair.sensor, pair.beacon),
      }));
  }
  async solveSilver(signalSensorPairs) {
    const range = signalSensorPairs.reduce(
      (range, sensorSignalPair) => {
        return [
          Math.min(
            range[0],
            sensorSignalPair.sensor[0] - sensorSignalPair.distance,
            sensorSignalPair.beacon[0]
          ),
          Math.max(
            range[1],
            sensorSignalPair.sensor[0] + sensorSignalPair.distance,
            sensorSignalPair.beacon[0]
          ),
        ];
      },
      [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY]
    );

    const positionsOfBeaconsAndSensors = new Set(
      signalSensorPairs
        .flatMap((pair) => [pair.beacon, pair.sensor])
        .map((coord) => coord.join(","))
    );
    const y = 2_000_000;
    let count = -1;

    for (let x = range[0]; x <= range[1]; x++) {
      let inSensorRange = false;

      for (let pair of signalSensorPairs) {
        if (pair.distance >= manhattanDistance(pair.sensor, [x, y])) {
          // We are in sensor range
          const verticalDiff = Math.abs(pair.sensor[1] - y);
          const horizontalDiff = pair.sensor[0] - x;

          inSensorRange = true;

          const jump = pair.distance - verticalDiff + horizontalDiff;
          x += jump;
          count += jump;

          break;
        }
      }
      if (inSensorRange || positionsOfBeaconsAndSensors.has(x + "," + y)) {
        count++;
      }
    }

    return count;
  }

  async solveGold(signalSensorPairs) {
    const positionsOfBeaconsAndSensors = new Set(
      signalSensorPairs
        .flatMap((pair) => [pair.beacon, pair.sensor])
        .map((coord) => coord.join(","))
    );

    for (let y = 4_000_000; y >= 0; y--) {
      for (let x = 0; x <= 4_000_000; x++) {
        let inSensorRange = false;

        for (let pair of signalSensorPairs) {
          if (pair.distance >= manhattanDistance(pair.sensor, [x, y])) {
            // We are in sensor range
            const verticalDiff = Math.abs(pair.sensor[1] - y);
            const horizontalDiff = pair.sensor[0] - x;

            inSensorRange = true;

            x += pair.distance - verticalDiff + horizontalDiff;

            break;
          }
        }
        if (inSensorRange || positionsOfBeaconsAndSensors.has(x + "," + y)) {
          continue;
        }

        return x * 4000000 + y;
      }
    }
  }
}
