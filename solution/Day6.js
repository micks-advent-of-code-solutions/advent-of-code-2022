import Solution from "./Solution.js";

export default class Day6 extends Solution {
  async prepareData(input) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input.split("");
  }

  solver(data, blockSize) {
    for (let i = 0; i < data.length - blockSize - 1; i++) {
      const blockToCheck = data.slice(i, i + blockSize);
      const uniqueChars = new Set(blockToCheck);
      if (uniqueChars.size === blockSize) {
        return i + blockSize;
      }
    }
  }

  async solveSilver(data) {
    return this.solver(data, 4);
  }

  async solveGold(data) {
    return this.solver(data, 14);
  }
}
