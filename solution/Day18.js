import Solution from "./Solution.js";
import { mapInt, Vector3, Grid3, GridVolume, toProduct } from "./utils.js";

export default class Day18 extends Solution {
  async prepareData(input, isSilver, isGold) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input
      .split("\n")
      .map((line) => line.split(",").map(mapInt))
      .map((coords) => new Vector3(...coords));
  }
  async solveSilver(cubes) {
    const cubeSet = new Set(cubes.map((cube) => cube.toString()));
    return (
      cubes.length * 6 -
      cubes.flatMap((cube) =>
        Grid3.getDirectNeighbors(cube)
          .map((v) => v.toString())
          .filter((v) => cubeSet.has(v))
      ).length
    );
  }

  async solveGold(cubes) {
    // To find the volume and ignore inside holes, we build a volume and use
    // floodfill to fill the outside area. Then we iterate over the full volume
    // everything not outside is our inside. After that we can use the same algorithm
    // as for silver
    const gridVolume = new GridVolume(cubes);
    // Add "border" to allow the floodfill to "flow" around the volume
    gridVolume.extendBounds(1);
    const cubesOutside = gridVolume.fill(gridVolume.bounds[0]);

    const insideArea = [];

    for (let x = gridVolume.bounds[0].x; x < gridVolume.bounds[1].x; x++) {
      for (let y = gridVolume.bounds[0].y; y < gridVolume.bounds[1].y; y++) {
        for (let z = gridVolume.bounds[0].z; z < gridVolume.bounds[1].z; z++) {
          if (!cubesOutside.has(x + "," + y + "," + z)) {
            insideArea.push(new Vector3(x, y, z));
          }
        }
      }
    }

    return this.solveSilver(insideArea);
  }
}
