import Solution from "./Solution.js";
import { mapInt } from "./utils.js";

const drawGrid = (grid) => {
  console.log(
    grid.map((slice) => slice.map((value) => value ?? ".").join("")).join("\n")
  );
};

const InBounds = (grid) => (x, y) =>
  x >= 0 && x < grid[0].length && y >= 0 && y < grid.length;

const SandDropper = (grid, sandStart) => () => {
  const inBounds = InBounds(grid);
  let sand = [...sandStart];
  while (true) {
    let newSand = sand;
    const [sx, sy] = sand;
    if (grid[sy][sx] == null) {
      if (grid[sy + 1]?.[sx] == null) {
        newSand = [sx, sy + 1];
      } else if (grid[sy + 1]?.[sx - 1] == null) {
        newSand = [sx - 1, sy + 1];
      } else if (grid[sy + 1]?.[sx + 1] == null) {
        newSand = [sx + 1, sy + 1];
      } else {
        // no moves left
        grid[sand[1]][sand[0]] = "s";
        return true;
      }
      if (!inBounds(...newSand)) {
        return false;
      }
    } else {
      // no moves left
      grid[sand[1]][sand[0]] = "s";
      return false;
    }
    sand = newSand;
  }
};

export default class Day14 extends Solution {
  async prepareData(input, isSilver, isGold) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    const coords = input
      .split("\n")
      .map((line) =>
        line.split(" -> ").map((coord) => coord.split(",").map(mapInt))
      );

    const top = Math.min(...coords.flat().map(([x, y]) => y), 0);
    const bottom =
      Math.max(...coords.flat().map(([x, y]) => y), 0) + (isGold ? 2 : 0);
    const height = bottom - top;
    const left = Math.min(
      ...coords.flat().map(([x, y]) => x),
      isGold ? 500 - (bottom - top) : 500
    );
    const right = Math.max(
      ...coords.flat().map(([x, y]) => x),
      isGold ? 500 + (bottom - top) : 500
    );
    const width = right - left;

    let topLeft = [left, top];
    let bottomRight = [right, bottom];

    if (isGold) {
      coords.push([
        [500 - height, bottom],
        [500 + height, bottom],
      ]);
    }
    console.log(coords);

    const mappedCoords = coords.map((lineSet) =>
      lineSet.map(([x, y]) => [x - topLeft[0], y - topLeft[1]])
    );

    const grid = new Array(height + 1)
      .fill()
      .map(() => new Array(width + 1).fill().map(() => null));

    mappedCoords.forEach((lineSet) =>
      lineSet.reduce((a, b) => {
        let [ax, ay] = a;
        const [bx, by] = b;

        while (ax !== bx || ay !== by) {
          grid[ay][ax] = "█";
          if (ax < bx) {
            ax++;
          } else if (ax > bx) {
            ax--;
          } else if (ay < by) {
            ay++;
          } else if (ay > by) {
            ay--;
          }
          grid[ay][ax] = "█";
        }
        return b;
      })
    );

    const sandStart = [500 - topLeft[0], 0 - topLeft[1]];

    return { coords, topLeft, bottomRight, width, height, grid, sandStart };
  }
  async solveSilver({ grid, sandStart }) {
    let sandDropped = 0;
    const dropSand = SandDropper(grid, sandStart);

    while (dropSand()) {
      sandDropped++;
    }
    drawGrid(grid);

    return sandDropped;
  }

  async solveGold({ grid, sandStart }) {
    let sandDropped = 0;
    const dropSand = SandDropper(grid, sandStart);

    while (dropSand()) {
      sandDropped++;
    }
    drawGrid(grid);
    return sandDropped;
  }
}
