// Reducer Helpers
export const toSum = (a, b) => a + b;
export const toProduct = (a, b) => a * b;

// Sort Helpers
export const sortAscending = (a, b) => b - a;
export const sortDesending = (a, b) => a - b;

// Map Helpers
export const mapInt = (num) => parseInt(num);

// Math Helpers
export const normalize = (val) => (val === 0 ? 0 : val / Math.abs(val));

export const distanceSquared = (a, b) =>
  (a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2;
export const distance = (a, b) => Math.sqrt(distanceSquared(a, b));
export const manhattanDistance = (a, b) =>
  Math.abs(a[0] - b[0]) + Math.abs(a[1] - b[1]);

export const rangeOf = (max, min = 0) =>
  Array(max - min)
    .fill()
    .map((x, i) => i + min);

export class Vector3 {
  static ONE = new Vector3(1, 1, 1);

  static fromString(str) {
    return Vector3.fromArray(str.split(","));
  }

  static fromArray([x, y, z]) {
    return new Vector3(x, y, z);
  }

  x = 0;
  y = 0;
  z = 0;

  constructor(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  get array() {
    return [this.x, this.y, this.z];
  }

  add(v3) {
    return new Vector3(...this.array.map((value, i) => value + v3.array[i]));
  }

  subtract(v3) {
    return new Vector3(...this.array.map((value, i) => value - v3.array[i]));
  }

  toString() {
    return this.x + "," + this.y + "," + this.z;
  }
}
export class Grid3 {
  static getDirectNeighbors(v3) {
    return [
      v3.add(new Vector3(0, 0, -1)),
      v3.add(new Vector3(0, 0, 1)),
      v3.add(new Vector3(0, -1, 0)),
      v3.add(new Vector3(0, 1, 0)),
      v3.add(new Vector3(-1, 0, 0)),
      v3.add(new Vector3(1, 0, 0)),
    ];
  }

  static getBoundingBox(vector3List) {
    const coordsByAxis = Array(3)
      .fill()
      .map(() => []);
    vector3List
      .map((v) => v.array)
      .forEach((pos) => pos.forEach((value, i) => coordsByAxis[i].push(value)));
    const bounds = [
      new Vector3(
        Math.min(...coordsByAxis[0]),
        Math.min(...coordsByAxis[1]),
        Math.min(...coordsByAxis[2])
      ),
      new Vector3(
        Math.max(...coordsByAxis[0]),
        Math.max(...coordsByAxis[1]),
        Math.max(...coordsByAxis[2])
      ),
    ];

    return bounds;
  }
}
export class GridVolume extends Grid3 {
  bounds = [new Vector3(0, 0, 0), new Vector3(0, 0, 0)];
  entities = [];

  constructor(entities, bounds = Grid3.getBoundingBox(entities)) {
    super();
    this.entities = entities;

    this.bounds = bounds;
  }

  extendBounds(size) {
    this.bounds = [
      this.bounds[0].subtract(Vector3.ONE),
      this.bounds[1].add(Vector3.ONE),
    ];
  }

  fill(startPosition) {
    const blockedIds = new Set(
      this.entities.map((entity) => entity.toString())
    );
    const visitedIds = new Set();
    const toVisitIds = new Set([startPosition]);
    const nodesToVisit = [startPosition];

    while (nodesToVisit.length > 0) {
      const nodeToCheck = nodesToVisit.shift();
      if (visitedIds.has(nodeToCheck.toString())) debugger;
      visitedIds.add(nodeToCheck.toString());
      const neighbors = this.getDirectNeighbors(nodeToCheck).filter(
        (position) => {
          const positionId = position.toString();
          return !(
            visitedIds.has(positionId) ||
            blockedIds.has(positionId) ||
            toVisitIds.has(positionId)
          );
        }
      );
      if (neighbors.length) {
        nodesToVisit.push(...neighbors);
        neighbors.forEach((n) => toVisitIds.add(n.toString()));
      }
    }
    return visitedIds;
  }

  getDirectNeighbors(v3) {
    return Grid3.getDirectNeighbors(v3).filter((v3) => {
      return v3.array.every(
        (value, axis) =>
          this.bounds[0].array[axis] <= value &&
          this.bounds[1].array[axis] >= value
      );
    });
  }
}
