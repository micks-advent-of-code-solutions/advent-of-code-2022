import Solution from "./Solution.js";
import { toProduct, toSum } from "./utils.js";

const isInRightOrder = (left, right) => {
  for (let i = 0; i < Math.max(left.length, right.length); i++) {
    const leftValue = left[i];
    const rightValue = right[i];

    if (leftValue === undefined) return true;
    if (rightValue === undefined) return false;

    if (Number.isInteger(leftValue) && Number.isInteger(rightValue)) {
      if (leftValue === rightValue) continue;
      if (leftValue < rightValue) return true;
      else return false;
    }
    if (Array.isArray(leftValue) || Array.isArray(rightValue)) {
      const result = isInRightOrder([leftValue].flat(), [rightValue].flat());
      if (result == null) continue;
      return result;
    }
  }
};

export default class Day13 extends Solution {
  async prepareData(input) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input
      .split("\n\n")
      .map((pairs) => pairs.split("\n").map((line) => JSON.parse(line)));
  }
  async solveSilver(signalPairs) {
    return signalPairs
      .map((pair, i) => ({
        index: i + 1,
        order: isInRightOrder(...pair),
      }))
      .filter(({ order }) => order)
      .map(({ index }) => index)
      .reduce(toSum);
  }

  async solveGold(signalPairs) {
    const markers = [[[2]], [[6]]];
    signalPairs.push(markers);

    const sortedSignals = signalPairs
      .flat() // We don't need pairs of signals, just a flat array
      .sort((a, b) => (isInRightOrder(a, b) ? -1 : 1)); // let's sort

    return markers
      .map((marker) => sortedSignals.indexOf(marker) + 1)
      .reduce(toProduct);
  }
}
