import Solution from "./Solution.js";
import { toSum } from "./utils.js";

class Directory {
  parent;
  children = {};

  constructor(parent) {
    this.parent = parent;
  }

  get size() {
    return Object.values(this.children)
      .map((entry) => entry.size)
      .reduce(toSum);
  }
}

class File {
  size;

  constructor(size) {
    this.size = size;
  }
}

class FileSystem {
  baseDir = new Directory("/", null);
  currentDir = this.baseDir;

  directories = [this.baseDir];

  addFile(name, size) {
    this.currentDir.children[name] = new File(size);
  }
  addDirectory(name) {
    this.currentDir.children[name] = new Directory(this.currentDir);
    this.directories.push(this.currentDir.children[name]);
  }
  navigate(to) {
    if (to === "/") {
      this.currentDir = this.baseDir;
    } else if (to === "..") {
      this.currentDir = this.currentDir.parent;
    } else {
      this.currentDir = this.currentDir.children[to];
    }
  }
}

export default class Day7 extends Solution {
  async prepareData(input) {
    const fileSystem = new FileSystem();

    input.split("\n").forEach((line) => {
      if (line.startsWith("$ cd")) {
        const to = line.split("$ cd ")[1];
        fileSystem.navigate(to);
      } else if (line.startsWith("dir")) {
        fileSystem.addDirectory(line.split("dir ")[1]);
      } else if (/^\d+/.test(line)) {
        const [size, name] = line.split(" ");
        fileSystem.addFile(name, parseInt(size));
      }
    });

    return fileSystem;
  }
  async solveSilver(fileSystem) {
    return fileSystem.directories
      .map((dir) => dir.size)
      .filter((size) => size <= 100000)
      .reduce(toSum);
  }

  async solveGold(fileSystem) {
    const total = 70000000;
    const needed = 30000000;
    const used = fileSystem.baseDir.size;
    const unused = total - used;
    const missing = needed - unused;

    return Math.min(
      ...fileSystem.directories
        .map((dir) => dir.size)
        .filter((size) => size >= missing)
    );
  }
}
