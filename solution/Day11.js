import Solution from "./Solution.js";
import { mapInt, sortDesending, toProduct } from "./utils.js";

const DividerTester = (divider, trueMonkey, falseMonkey) => (value) =>
  value % divider === 0 ? trueMonkey : falseMonkey;

const Operation = (op) => new Function("old", `return ${op}`);

export default class Day11 extends Solution {
  async prepareData(input) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input.split("\n\n").map((monkey) => {
      const monkeyData = {};
      const monkeyLines = monkey
        .split("\n")
        .map((line) => line.split(":").pop().trim());

      const divider = parseInt(monkeyLines[3].split("divisible by ").pop());
      return {
        items: monkeyLines[1].split(", ").map(mapInt),

        operation: Operation(monkeyLines[2].split("= ").pop()),
        test: DividerTester(
          divider,
          ...monkeyLines
            .slice(4)
            .map((line) => parseInt(line.split("monkey ").pop()))
        ),
        itemsInspected: 0,
        divider,
      };
    });
  }
  async solveSilver(monkeys) {
    for (let round = 0; round < 20; round++) {
      monkeys.forEach((monkey) => {
        while (monkey.items.length) {
          monkey.itemsInspected++;
          const item = monkey.items.shift();
          const newItemValue = Math.floor(monkey.operation(item) / 3);

          monkeys[monkey.test(newItemValue)].items.push(newItemValue);
        }
      });
    }
    return monkeys
      .map((monkey) => monkey.itemsInspected)
      .sort(sortDesending)
      .slice(0, 2)
      .reduce(toProduct);
  }

  async solveGold(monkeys) {
    const commonDivider = monkeys.map((m) => m.divider).reduce(toProduct);
    for (let round = 0; round < 10000; round++) {
      monkeys.forEach((monkey) => {
        while (monkey.items.length) {
          monkey.itemsInspected++;
          const item = monkey.items.shift();
          const newItemValue = monkey.operation(item) % commonDivider;
          monkeys[monkey.test(newItemValue)].items.push(newItemValue);
        }
      });
    }
    return monkeys
      .map((monkey) => monkey.itemsInspected)
      .sort(sortDesending)
      .slice(0, 2)
      .reduce(toProduct);
  }
}
