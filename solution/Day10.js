import Solution from "./Solution.js";
import { mapInt, toSum } from "./utils.js";

class Cpu {
  cycle = 0;
  registerX = 1;
  instructionCycle = 0;
  programPosition = -1;
  currentInstruction = null;
  program = [];

  instructions = {
    noop: { cycles: 1 },
    addx: {
      cycles: 2,
      onCompletion(value) {
        this.registerX += value;
      },
    },
  };

  constructor(program) {
    this.program = program;
  }

  runCycles(cycleAmount = 1, print = false) {
    for (let i = 0; i < cycleAmount; i++) {
      if (print) this.printState();
      if (this.instructionCycle === 0) {
        if (this.currentInstruction) {
          const completionHook =
            this.instructions[this.currentInstruction.instruction].onCompletion;
          if (completionHook) {
            completionHook.bind(this)(...this.currentInstruction.args);
          }
        }

        // advance to next instuction
        this.programPosition = (this.programPosition + 1) % this.program.length;
        this.currentInstruction = this.program[this.programPosition];
        this.instructionCycle =
          this.instructions[this.currentInstruction.instruction].cycles;
      }
      this.cycle++;
      if (this.instructionCycle > 0) {
        this.instructionCycle--;
      }
    }
  }

  printState() {
    const { registerX, currentInstruction, cycle } = this;
    console.log({
      registerX,
      currentInstruction,
      cycle,
    });
  }
}

export default class Day10 extends Solution {
  async prepareData(input) {
    return input
      .split("\n")
      .map((line) => line.split(" "))
      .map(([instruction, ...args]) => ({
        instruction,
        args: args.map(mapInt),
      }));
  }
  async solveSilver(program) {
    const signalStrenghts = [];
    const cpu = new Cpu(program);

    cpu.runCycles(20);
    signalStrenghts.push(cpu.cycle * cpu.registerX);
    for (let i = 0; i < 5; i++) {
      cpu.runCycles(40);
      signalStrenghts.push(cpu.cycle * cpu.registerX);
    }
    return signalStrenghts.reduce(toSum);
  }

  async solveGold(program) {
    const cpu = new Cpu(program);
    const screen = [];
    for (let y = 0; y < 6; y++) {
      const line = [];
      for (let x = 0; x < 40; x++) {
        cpu.runCycles();
        line.push(Math.abs(cpu.registerX - x) < 2 ? "█" : " ");
      }
      screen.push(line.join(""));
    }
    return "\n" + screen.join("\n");
  }
}
