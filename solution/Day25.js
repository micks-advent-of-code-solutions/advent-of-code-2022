import Solution from "./Solution.js";
import { toSum } from "./utils.js";

const parseSnafu = (str) =>
  str
    .split("")
    .reverse()
    .map((snafuNum) => ({ "=": -2, "-": -1 }[snafuNum] ?? parseInt(snafuNum)))
    .map((num, i) => num * 5 ** i)
    .reduce(toSum);

// For conversion to snafu, keep dividing by 5, take remainder as digits of new number.
// Convert to - 1, -2 if remainder >= 3 and also add one to quotient.
const toSnafu = (number) => {
  const snafuOf = { 5: "0", 4: "-", 3: "=" };
  let current = number;
  let snafu = [];
  while (current > 0) {
    const remainder = current % 5;
    const divident = Math.floor(current / 5);
    snafu.unshift(snafuOf[remainder] ?? remainder);
    if (remainder > 2) {
      current = divident + 1;
    } else {
      current = divident;
    }
  }
  return snafu.join("");
};

export default class Day25 extends Solution {
  async prepareData(input, isSilver, isGold) {
    return input.split("\n").map(parseSnafu);
  }
  async solveSilver(numbers) {
    return toSnafu(numbers.reduce(toSum));
  }

  async solveGold(data) {
    return "Solved by completing everything";
  }
}
