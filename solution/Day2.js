import Solution from './Solution.js';
import { toSum } from './utils.js';

export default class Day2 extends Solution {
  async prepareData(input) {
    return input.split('\n').map((entry) => entry.split(' '));
  }
  async solveSilver(data) {
    const scores = data.map(([opponent, me]) => {
      const [opponentValue, meValue] = [
        opponent.charCodeAt(0) - 'A'.charCodeAt(0) + 1,
        me.charCodeAt(0) - 'X'.charCodeAt(0) + 1,
      ];

      // This is horrible, but it works :/
      if (
        (meValue > opponentValue && !(opponentValue === 1 && meValue === 3)) ||
        (meValue === 1 && opponentValue === 3)
      ) {
        return meValue + 6;
      }
      if (meValue === opponentValue) {
        return meValue + 3;
      }
      return meValue;
    });
    return scores.reduce(toSum);
  }

  async solveGold(data) {
    const scores = data.map(([opponent, me]) => {
      const [opponentValue, condition] = [
        opponent.charCodeAt(0) - 'A'.charCodeAt(0),
        me.charCodeAt(0) - 'X'.charCodeAt(0),
      ];

      return ((opponentValue + condition - 1 + 3) % 3) + 1 + condition * 3;
    });

    return scores.reduce(toSum);
  }
}
