// Mostly stolen from AoC 2021 Day 15 :)
import Solution from "./Solution.js";

function sortNodes(a, b) {
  if (a.distance === b.distance) {
    // If same distance, sort by distance from center line :)
    return Math.abs(a.x - a.y) - Math.abs(b.x - b.y);
  } else {
    return a.distance - b.distance;
  }
}

export default class Day12 extends Solution {
  async prepareData(input) {
    this.grid = input.split("\n").map((line, y) =>
      line.split("").map((entry, x) => {
        const node = {
          name: entry,
          level:
            (entry === "S"
              ? "a".charCodeAt(0)
              : entry === "E"
              ? "z".charCodeAt(0)
              : entry.charCodeAt(0)) - "a".charCodeAt(0),
          x,
          y,
          distance: Number.MAX_SAFE_INTEGER,
          solved: false,
        };
        if (entry === "E") this.end = node;
        return node;
      })
    );
    this.width = this.grid[0].length;
    this.height = this.grid.length;
  }

  getSurroundingNodes({ x, y }) {
    return [
      [x - 1, y],
      [x + 1, y],
      [x, y - 1],
      [x, y + 1],
    ]
      .filter(([x, y]) => x > -1 && y > -1 && x < this.width && y < this.height)
      .filter(([nx, ny]) => this.grid[ny][nx].level - this.grid[y][x].level < 2)
      .map(([x, y]) => this.grid[y][x]);
  }

  dutchDude(startNode) {
    const nodes = [];
    const goal = this.end;
    let node = startNode;

    // reset grid
    this.grid.flat().forEach((node) => {
      node.distance = Number.MAX_VALUE;
      node.solved = false;
    });

    startNode.distance = 0;
    startNode.solved = true;

    while (node != goal) {
      this.getSurroundingNodes(node)
        .filter((node) => !node.solved)
        .forEach((surroundingNode) => {
          // node is interesting, save in list
          if (!nodes.includes(surroundingNode)) nodes.push(surroundingNode);
          const newDistance = node.distance + 1;
          if (newDistance < surroundingNode.distance) {
            surroundingNode.distance = newDistance;
            surroundingNode.previous = node;
          }
        });

      nodes.sort(sortNodes);
      node.solved = true;
      node = nodes.shift();
      if (!node) {
        return null;
      }
    }

    return goal;
  }

  drawRoute(node) {
    const grid = Array(this.height)
      .fill()
      .map((_, y) =>
        Array(this.width)
          .fill()
          .map((_, x) => " " + this.grid[y][x].level + " ")
      );
    let currentNode = node;
    while (currentNode) {
      grid[currentNode.y][currentNode.x] = "[" + currentNode.level + "]";
      currentNode = currentNode.previous;
    }
    console.log(grid.map((row) => row.join("")).join("\n"));
  }

  async solveSilver() {
    const route = this.dutchDude(
      this.grid.flat().find((node) => node.name === "S")
    );

    return route.distance;
  }

  async solveGold(data) {
    const startNodes = this.grid.flat().filter((node) => node.level === 0);

    const distances = startNodes
      .map((startNode, i) => this.dutchDude(startNode)?.distance)
      .filter((distance) => distance != null);

    return Math.min(...distances);
  }
}
