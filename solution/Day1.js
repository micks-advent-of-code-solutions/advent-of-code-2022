import Solution from "./Solution.js";
import { mapInt, sortAscending, toSum } from "./utils.js";

export default class Day1 extends Solution {
  async prepareData(input) {
    const data = input
      .split("\n\n")
      .map((entry) => entry.split("\n").map(mapInt));

    return data.map((elf) => elf.reduce(toSum));
  }
  async solveSilver(addedCalories) {
    return Math.max(...addedCalories);
  }

  async solveGold(addedCalories) {
    return addedCalories.sort(sortAscending).slice(0, 3).reduce(toSum);
  }
}
