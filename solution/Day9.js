import Solution from "./Solution.js";
import { normalize, distanceSquared } from "./utils.js";

const stepMap = {
  L: [-1, 0],
  R: [1, 0],
  U: [0, -1],
  D: [0, 1],
};

class RopeKnot {
  position = [0, 0];
  nextKnot = null;
  positionTracker = null;

  constructor(positionTracker, ropeLength) {
    if (ropeLength) {
      this.nextKnot = new RopeKnot(positionTracker, ropeLength - 1);
    } else {
      // We are the tail
      this.positionTracker = positionTracker;
      // Track startPosition
      this.move(this.position);
    }
  }

  stepDirection(direction) {
    this.move(stepMap[direction]);
  }

  move([x, y]) {
    this.position[0] += x;
    this.position[1] += y;

    if (this.nextKnot) {
      this.nextKnot.solve(this);
    } else {
      // We are the tail
      this.positionTracker.add(this.position.join("|"));
    }
  }

  solve(previousKnot) {
    if (distanceSquared(previousKnot.position, this.position) > 2) {
      this.move([
        normalize(previousKnot.position[0] - this.position[0]),
        normalize(previousKnot.position[1] - this.position[1]),
      ]);
    }
  }
}

export default class Day9 extends Solution {
  async prepareData(input) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input
      .split("\n")
      .map((line) => line.split(" "))
      .map(([direction, count]) => ({ direction, count: parseInt(count) }));
  }

  async solveSilver(steps) {
    const visitedPlaces = new Set();
    const rope = new RopeKnot(visitedPlaces, 1);

    steps.forEach(({ direction, count }) => {
      for (let i = 0; i < count; i++) {
        rope.stepDirection(direction);
      }
    });

    return visitedPlaces.size;
  }

  async solveGold(steps) {
    const visitedPlaces = new Set();
    const rope = new RopeKnot(visitedPlaces, 9);

    steps.forEach(({ direction, count }) => {
      for (let i = 0; i < count; i++) {
        rope.stepDirection(direction);
      }
    });

    return visitedPlaces.size;
  }
}
