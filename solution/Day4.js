import Solution from "./Solution.js";
import { mapInt } from "./utils.js";

const contains = (range, containedRange) =>
  containedRange[0] >= range[0] && containedRange[1] <= range[1];

const containsEither = (rangeA, rangeB) =>
  contains(rangeA, rangeB) || contains(rangeB, rangeA);

const isBetween = (range, test) => range[0] <= test && range[1] >= test;

const overlaps = (rangeA, rangeB) =>
  isBetween(rangeA, rangeB[0]) ||
  isBetween(rangeA, rangeB[1]) ||
  isBetween(rangeB, rangeA[0]) ||
  isBetween(rangeB, rangeA[1]);

export default class Day4 extends Solution {
  async prepareData(input) {
    return input
      .split("\n")
      .map((line) =>
        line.split(",").map((entry) => entry.split("-").map(mapInt))
      );
  }

  async solveSilver(data) {
    return data.filter((ranges) => containsEither(...ranges)).length;
  }

  async solveGold(data) {
    return data.filter((ranges) => overlaps(...ranges)).length;
  }
}
