import Solution from "./Solution.js";

export default class Day5 extends Solution {
  async prepareData(input) {
    const instrcutionRex =
      /move (?<amount>\d+) from (?<from>\d+) to (?<to>\d+)/;

    const [initialStateBlock, instructionsBlock] = input.split("\n\n");

    const instructions = instructionsBlock
      .split("\n")
      .map((line) =>
        Object.fromEntries(
          Object.entries(instrcutionRex.exec(line).groups).map(([k, v]) => [
            k,
            parseInt(v),
          ])
        )
      );

    const initialStateLines = initialStateBlock.split("\n");
    initialStateLines.pop(); // We don't need that numbers line

    const slots = (initialStateLines[0].length + 1) / 4;

    const initialState = Array(slots)
      .fill()
      .map(() => []);

    initialStateLines.forEach((line) => {
      for (let i = 0; i < slots; i++) {
        const value = line[i * 4 + 1];
        if (value !== " ") {
          initialState[i].push(value);
        }
      }
    });

    return { initialState, instructions };
  }
  async solveSilver({ initialState, instructions }) {
    const state = JSON.parse(JSON.stringify(initialState));
    instructions.forEach((instruction) => {
      for (let i = 0; i < instruction.amount; i++) {
        state[instruction.to - 1].unshift(state[instruction.from - 1].shift());
      }
    });
    return state.map((slot) => slot.shift()).join("");
  }

  async solveGold({ initialState, instructions }) {
    const state = JSON.parse(JSON.stringify(initialState));
    instructions.forEach((instruction) => {
      state[instruction.to - 1].unshift(
        ...state[instruction.from - 1].splice(0, instruction.amount)
      );
    });
    return state.map((slot) => slot.shift()).join("");
  }
}
