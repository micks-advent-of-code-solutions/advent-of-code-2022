import Solution from "./Solution.js";

export default class Day19 extends Solution {
  async prepareData(input, isSilver, isGold) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input.split("\n").map((line) =>
      line
        .split(": ")
        .pop()
        .split(". ")
        .map((robotDefinition) => {
          const [produces, costsDefinition] = robotDefinition
            .split("Each ")
            .pop()
            .split(" robot costs ");

          const costs = costsDefinition.split(" and ").map((cost) => {
            const [amount, material] = cost.split(" ");
            return { amount: parseInt(amount), material };
          });

          return { produces, costs };
        })
    );
  }
  async solveSilver(data) {
    return JSON.stringify(data, null, " ");
  }

  async solveGold(data) {
    return "Not solved Yet!";
  }
}
