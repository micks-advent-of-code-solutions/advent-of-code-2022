import Solution from "./Solution.js";

export default class Day16 extends Solution {
  async prepareData(input, isSilver, isGold) {
    return Object.fromEntries(
      input.split("\n").map((line) => {
        const { name, flowRate, connections } = line.match(
          /^Valve (?<name>[A-Z]{2}) has flow rate=(?<flowRate>\d+); tunnel[s]? lead[s]? to valve[s]? (?<connections>.*)$/
        ).groups;

        return [
          name,
          {
            name,
            flowRate: parseInt(flowRate),
            connections: connections.split(", "),
          },
        ];
      })
    );
  }
  async solveSilver(data) {
    return Object.values(data)
      .flatMap((relation) =>
        relation.connections.map((c) => `${relation.name} --> ${c}`)
      )
      .join("\n");
  }

  async solveGold(data) {
    return "Not solved Yet!";
  }
}
