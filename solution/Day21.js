import Solution from "./Solution.js";

class MonkeySolver {
  operations = {
    "+": (a, b) => a + b,
    "-": (a, b) => a - b,
    "*": (a, b) => a * b,
    "/": (a, b) => a / b,
  };

  // left +-*/ complexTerm = value
  leftSwap = {
    "+": "-",
    "-": "-",
    "*": "/",
    "/": "/",
  };
  // complexTerm +-*/ right = value
  rightSwap = {
    "+": "-",
    "-": "+",
    "*": "/",
    "/": "*",
  };

  swapValues(operant) {
    return operant === "-" || operant === "/";
  }

  constructor(monkeys) {
    this.monkeys = monkeys;
  }

  solve(instruction) {
    const left = this.get(instruction.left);
    const right = this.get(instruction.right);
    if (left == null || right == null) {
      return null;
    }
    return this.operations[instruction.operant](
      this.get(instruction.left),
      this.get(instruction.right)
    );
  }

  solveEquation(key, value) {
    //console.log(this.expand(key), "=", value);
    const instruction = this.monkeys[key].instruction;
    const leftKey = instruction.left;
    const rightKey = instruction.right;
    const leftResult = this.monkeys[leftKey].result;
    const rightResult = this.monkeys[rightKey].result;

    const hasLeftResult = leftResult != null && leftResult !== "x";

    const result = hasLeftResult ? leftResult : rightResult;

    let newValue;
    const operatorMapping = hasLeftResult ? this.leftSwap : this.rightSwap;
    if (this.swapValues(instruction.operant)) {
      newValue = this.operations[operatorMapping[instruction.operant]](
        result,
        value
      );
    } else {
      newValue = this.operations[operatorMapping[instruction.operant]](
        value,
        result
      );
    }
    if (leftResult === "x" || rightResult === "x") return newValue;
    return this.solveEquation(leftResult ? rightKey : leftKey, newValue);
  }

  get(key) {
    const monkey = this.monkeys[key];
    if (monkey.result == "x") return null;
    if (monkey.result == null) monkey.result = this.solve(monkey.instruction);
    return monkey.result;
  }

  expand(key) {
    const monkey = this.monkeys[key];
    if (monkey.result != null) return monkey.result;
    else
      return (
        "(" +
        this.expand(monkey.instruction.left) +
        monkey.instruction.operant +
        this.expand(monkey.instruction.right) +
        ")"
      );
  }
}

export default class Day21 extends Solution {
  async prepareData(input, isSilver, isGold) {
    const numberRex = /\d+/;
    const instructionRex = /(?<left>\w+) (?<operant>[-+*\/]) (?<right>\w+)/;
    return Object.fromEntries(
      input.split("\n").map((line) => {
        const [name, value] = line.split(": ");
        const info = {};
        if (numberRex.test(value)) info.result = parseInt(value);
        else info.instruction = instructionRex.exec(value).groups;
        return [name, info];
      })
    );
  }
  async solveSilver(monkeys) {
    return new MonkeySolver(monkeys).get("root");
  }

  async solveGold(monkeys) {
    monkeys["humn"].result = "x";
    const solver = new MonkeySolver(monkeys);
    // simplify the terms as far as possible, ignoring x
    solver.get("root");
    // resolve the left part of the equation until x is solved (since x is left for my data)
    return solver.solveEquation(
      monkeys["root"].instruction.left,
      monkeys[monkeys["root"].instruction.right].result
    );
  }
}
