import Solution from "./Solution.js";

const shapes = `####

.#.
###
.#.

..#
..#
###

#
#
#
#

##
##`
  .split("\n\n")
  .map((shape) =>
    shape.split("\n").flatMap((line, y, { length: height }) =>
      line
        .split("")
        .map((value, x) => (value === "#" ? [x, y - height + 1] : null))
        .filter((x) => x)
    )
  );

export default class Day17 extends Solution {
  async prepareData(input, isSilver, isGold) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input.split("").map((char) => (char === ">" ? 1 : -1));
  }
  async solveSilver(wind) {
    return shapes;
  }

  async solveGold(data) {
    return "Not solved Yet!";
  }
}
