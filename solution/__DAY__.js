import Solution from "./Solution.js";

export default class __DAY__ extends Solution {
  async prepareData(input, isSilver, isGold) {
    // Parse / transform data here, will be provided in solve(Silver|Gold)
    return input;
  }
  async solveSilver(data) {
    return ["Not solved Yet!", data];
  }

  async solveGold(data) {
    return "Not solved Yet!";
  }
}
