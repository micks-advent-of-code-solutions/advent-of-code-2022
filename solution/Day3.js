import Solution from './Solution.js';
import { toSum } from './utils.js';

const charToValue = (char) => {
  const charCode = char.charCodeAt(0);
  return charCode > 96 ? charCode - 96 : charCode - 64 + 26;
};

const union = (aSet, bSet) =>
  [...bSet.values()].filter((entry) => aSet.has(entry));

export default class Day3 extends Solution {
  async prepareData(input) {
    return input.split('\n').map((line) => line.split(''));
  }
  async solveSilver(data) {
    return data
      .map((rucksack) =>
        [
          rucksack.slice(0, rucksack.length / 2),
          rucksack.slice(rucksack.length / 2),
        ].map((part) => new Set(part))
      )
      .flatMap(([left, right]) => union(left, right))
      .map(charToValue)
      .reduce(toSum);
  }

  async solveGold(data) {
    const badges = [];
    for (let i = 0; i < data.length; i += 3) {
      const group = data.slice(i, i + 3);
      badges.push(
        group.reduce((a, b) => {
          return union(new Set(a), new Set(b));
        })
      );
    }
    return badges.flat().map(charToValue).reduce(toSum);
  }
}
